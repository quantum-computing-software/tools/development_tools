#!/usr/bin/env bash

################################################################################
# Check the existing metadata files
################################################################################

# all possible options for this script
OPTION_KEEP="keep"
REMOVE=true

# get and go to the repo root
REPO=$(git rev-parse --show-superproject-working-tree --show-toplevel | head -1)
cd $REPO

# get the functions and global settings of the development_tools
source modules/development_tools/build/funcs.sh
source modules/development_tools/build/settings.sh

# get the (local, possibly overwriting) project settings
SETTINGS=dev_tools/settings.sh
if [ -f "$SETTINGS" ]; then
    source $SETTINGS
else
    echo "$SETTINGS does not exist."
fi

# if there are parameters given we loop through them
while [ "$1" != "" ]; do
    case "$1" in
        # check if the given parameters fit to a run option
        # and set variables accordingly
        "$OPTION_KEEP") REMOVE=false;;
        *) echo "set test dir to $1"
           METADATA_TEST_DIR="$1";;
    esac
    # shift all the parameters down by one
    shift
done

# call the python script to create all files and pass all options
python modules/development_tools/build/metadata.py "$METADATA_TEST_DIR/"

SUCCESS=0
echo "check files against the created ones based on METADATA.yaml"

for FILE in "README.md" "setup.py" "CITATION.cff"; do
    # compare generated files against repo file
    if cmp -s "$METADATA_TEST_DIR/$FILE" "$FILE"; then
        echo "The file $FILE is valid"
    else
        echo "The file $FILE is not valid"
        SUCCESS=1
    fi
done

# compare generated file against repo file
if cmp -s "$METADATA_TEST_DIR/meta.yaml" "$CONDA_BUILD_DIR/meta.yaml"; then
    echo "The file $CONDA_BUILD_DIR/meta.yaml is valid"
else
    echo "The file $CONDA_BUILD_DIR/meta.yaml is not valid"
    SUCCESS=1
fi

# clean up generated files if keep option is not set
if $REMOVE; then
    rm -r "$METADATA_TEST_DIR"
fi

# if some file is not valid, print suggestion
if [ $SUCCESS == 1 ]; then
    echo "Locally run 'bash run create_metadata' or download artifacts of this job to update metadata"
fi
exit $SUCCESS
