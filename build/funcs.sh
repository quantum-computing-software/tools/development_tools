#!/usr/bin/env bash
#
# functions for version check
#
# The version number of the package is retrieved and compared against the latest conda release version number.
# If the version number has not been increased, the check fails.
#

function get_package() {
    # Obtain the package name from the metadata.
    # Returns an error if the metadata file cannot be found.

    METADATA_FILE=$1

    if [ -e "$METADATA_FILE" ]; then
        PACKAGE=$(grep -m 1 'name:' "$METADATA_FILE" | grep -Eo '[a-zA-Z_]+' | tail -1)
    else
        echo "Cannot find metadata file $METADATA_FILE"
        exit 1
    fi
}

function get_new_version_number() {
    # Obtain newest version number from the metadata.
    # Returns an error if the metadata file cannot be found.

    METADATA_FILE=$1

    if [ -e "$METADATA_FILE" ]; then
        V_NEW=$(grep 'version:' "$METADATA_FILE" | grep -Eo '[0-9]+([.][0-9])+([0-9])?')
    else
        echo "Cannot find metadata file $METADATA_FILE"
        exit 1
    fi
}

function get_old_version_number() {
    # Obtain version number of latest conda release via conda search.
    # Returns an error if the package cannot be located in the specified conda channel.

    PACKAGE=$1
    CHANNEL=$2

    conda config --add channels "$CHANNEL"
    V_OLD=$(conda search "$CHANNEL"::"$PACKAGE" | tail -1 | cut -c 30-36)

    if [ "$(conda search "$CHANNEL"::"$PACKAGE" | tail -1 | cut -c 1-8)" = "No match" ]; then
        echo "Cannot find package $PACKAGE on conda channel $CHANNEL"
        exit 1
    fi
}

function check_version() {
    # Retrieves old and new version number.
    # Reads out their digits before and after the comma (integer and decimal value) and compares the version numbers.
    # Returns errors if the version numbers cannot be retrieved or are not increased.

    CHANNEL=$1
    METADATA_FILE=$2

    get_package "$METADATA_FILE"
    get_old_version_number "$PACKAGE" "$CHANNEL"
    get_new_version_number "$METADATA_FILE"

    # Split the float into its components (i.e., the numbers before and after the dot).
    # This is needed for proper version comparison, e.g., 1.12 > 1.3.
    V_NEW_INT=$(printf "%.0f" "$V_NEW")
    V_NEW_DEC=$(echo "$V_NEW" | grep -Eo '([.][0-9])+([0-9])?' | cut -c 2-)

    V_OLD_INT=$(printf "%.0f" "$V_OLD")
    V_OLD_DEC=$(echo "$V_OLD" | grep -Eo '([.][0-9])+([0-9])?' | cut -c 2-)

    # Perform the version number check.
    # Return error if version numbers are empty or not updated or smaller than the current release.
    if [ -z "$V_OLD" ]; then
        echo "Cannot retrieve current version from conda"
        exit 1
    fi

    if [ "$V_NEW_INT" -eq "$V_OLD_INT" ] && [ "$V_NEW_DEC" -eq "$V_OLD_DEC" ]; then
        echo "The version of $PACKAGE was not updated: old $V_OLD_INT.$V_OLD_DEC = new $V_NEW_INT.$V_NEW_DEC"
        exit 2
    elif [ "$V_NEW_INT" -lt "$V_OLD_INT" ] || ([ "$V_NEW_INT" -le "$V_OLD_INT" ] && [ "$V_NEW_DEC" -lt "$V_OLD_DEC" ]); then
        echo "The version of $PACKAGE was not increased: old $V_OLD_INT.$V_OLD_DEC > new $V_NEW_INT.$V_NEW_DEC"
        exit 1
    fi

    echo "Version update of $PACKAGE is fine: old $V_OLD_INT.$V_OLD_DEC < new $V_NEW_INT.$V_NEW_DEC"

    export VERSION="$V_NEW_INT.$V_NEW_DEC"
    export PACKAGE
    export CHANNEL
}
