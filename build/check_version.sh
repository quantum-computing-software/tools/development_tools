#!/usr/bin/env bash

################################################################################
# Run version check
################################################################################

# get and go to the repo root
REPO=$(git rev-parse --show-superproject-working-tree --show-toplevel | head -1)
cd $REPO


# get the functions and global settings of the development_tools
source modules/development_tools/build/funcs.sh
source modules/development_tools/build/settings.sh

# get the (local, possibly overwriting) project settings
SETTINGS=dev_tools/settings.sh
if [ -f "$SETTINGS" ]; then
    source $SETTINGS
else
    echo "$SETTINGS does not exist."
fi


# call check function
check_version "$CHANNEL" "$METADATA_FILE"
