#!/usr/bin/env bash

################################################################################
# Build conda package
################################################################################

# get and go to the repo root
REPO=$(git rev-parse --show-superproject-working-tree --show-toplevel | head -1)
cd $REPO


# get the functions and global settings of the development_tools
source modules/development_tools/build/settings.sh

# get the (local, possibly overwriting) project settings
SETTINGS=dev_tools/settings.sh
if [ -f "$SETTINGS" ]; then
    source $SETTINGS
else
    echo "$SETTINGS does not exist."
fi


mkdir -p ../conda_build/

conda config --add channels conda-forge
conda config --remove channels defaults

# if no parameter is given, we build locally
if [ "$1" == "" ]; then
    echo "test conda build locally"
    conda build "$CONDA_BUILD_DIR" --croot ../conda_build/ -c dlr-sc
else
    # otherwise we use the provided token to upload the build
    echo "build and upload conda"
    conda config --set anaconda_upload yes
    conda build "$CONDA_BUILD_DIR" --croot ../conda_build/ --user "$CHANNEL" --token "$1" -c dlr-sc
fi
