#!/usr/bin/env bash
#
# general settings for build
#

# conda channel
CHANNEL="dlr-sc"

# path to metadata file
METADATA_FILE="METADATA.yaml"

# output path for conda build recipe meta file
CONDA_BUILD_DIR="dev_tools/conda_build_recipe/"

# test dir for metadata
METADATA_TEST_DIR="test_metadata/"
