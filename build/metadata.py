""" utility for automatic creation of all metadata files """

import os
import sys
from datetime import datetime
from string import Template
import yaml


METADATA_YAML     = "METADATA.yaml"
CITATION_CFF      = "CITATION.cff"
SETUP_PY          = "setup.py"
README_MD         = "README.md"
CONF_PY           = "conf.py"
INDEX_HEADER_RST  = "index_header.rst"
META_YAML         = "meta.yaml"
REQUIREMENTS_TXT  = "requirements.txt"
DESCRIPTION_MD    = "docs/description.md"
LOCAL_SETTINGS_SH = "dev_tools/settings.sh"

MODULES_DIR   = "modules/"
DEV_TOOLS_ENV_SETTINGS_SH = MODULES_DIR + "development_tools/environment/settings.sh"
TEMPLATES_DIR = MODULES_DIR + "development_tools/templates/for_automatic_generation/"
TEMPLATE_CITATION_CFF     = TEMPLATES_DIR + CITATION_CFF
TEMPLATE_SETUP_PY         = TEMPLATES_DIR + SETUP_PY
TEMPLATE_README_MD        = TEMPLATES_DIR + README_MD
TEMPLATE_CONF_PY          = TEMPLATES_DIR + CONF_PY
TEMPLATE_INDEX_HEADER_RST = TEMPLATES_DIR + INDEX_HEADER_RST

CONDA_BUILD_RECIPE_BASE_YAML = TEMPLATES_DIR + "conda_build_recipe_base.yaml"
CONDA_BUILD_RECIPE_DIR = "dev_tools/conda_build_recipe/"
CONDA_BUILD_RECIPE_META_YAML = CONDA_BUILD_RECIPE_DIR + META_YAML

DOCS_BUILD_DIR        = MODULES_DIR + "development_tools/docs/pages/source/"
DOCS_CONF_PY          = DOCS_BUILD_DIR + CONF_PY
DOCS_INDEX_HEADER_RST = DOCS_BUILD_DIR + INDEX_HEADER_RST


REQUIRED_METADATA = {"package": ["name", "version"],
                     "about"  : ["home", "license", "license_family", "license_file",
                                 "summary", "doc_url", "dev_url", "keywords"],
                     "author" : ["name", "alias", "website", "affiliation", "email"]}

CONTENT_SETUP = [('package', 'name'),
                 ('package', 'version'),
                 ('about', 'home'),
                 ('author', 'name'),
                 ('author', 'email'),
                 ('about', 'summary')]
CONTENT_CITATION = [('about', 'title'),
                    ('author', 'name'),
                    ('author', 'alias'),
                    ('author', 'website'),
                    ('author', 'affiliation'),
                    ('author', 'email'),
                    ('about', 'home'),
                    ('about', 'doc_url'),
                    ('about', 'summary'),
                    ('about', 'keywords'),
                    ('about', 'license'),
                    ('package', 'version'),
                    ('package', 'released'),
                    ('package','doi')]
CONTENT_README = [('about', 'title'),
                  ('about', 'home'),
                  ('about', 'summary'),
                  ('about', 'doc_url'),
                  ('about', 'description'),
                  ('about', 'license'),
                  ('author', 'affiliation')]
CONTENT_CONF = [('package', 'name'),
                ('package', 'version'),
                ('author', 'name'),
                ('author', 'affiliation')]
CONTENT_INDEX_HEADER = [('about', 'title'),
                        ('about', 'summary')]

class Metadata():
    """ Class for handling the metadata of a python project """

    def __init__(self, metadata_file_path=METADATA_YAML):
        """
        The basic metadata is loaded from the given file

        ::param (str) metadata_file_path: path to the metadata file
        """
        with open(metadata_file_path, 'r') as metadata_file:
            self.data = yaml.safe_load(metadata_file)

        self._check_metadata()

    def _check_metadata(self):
        """ Check the metadata if it contains all necessary keywords """
        for key0 in REQUIRED_METADATA:
            if key0 not in self.data:
                raise ValueError(f"Metadata not complete: key '{key0}' missing")
            for key1 in REQUIRED_METADATA[key0]:
                if key1 not in self.data[key0]:
                    raise ValueError(f"Metadata not complete: key '{key1}' missing in '{key0}'")

    # add further necessary data from different files

    def add_conda_build_recipe_base(self, conda_build_recipe_base_file_path=CONDA_BUILD_RECIPE_BASE_YAML):
        """
        Get and add the base data for the conda build recipe meta file

        :param conda_build_recipe_base_file_path: path to input file
        """
        with open(conda_build_recipe_base_file_path, 'r') as base_file:
            base_data = yaml.safe_load(base_file)
        self.data.update(base_data)

        # overwrite with actual package name
        self.data['test']['import'] = [self.data['package']['name']]

    def add_python_version(self, local_settings_path=LOCAL_SETTINGS_SH):
        """ get the python version of the package from the settings file """
        self.data['requirements']['run'] = [get_python_version(local_settings_path)]

    def add_requirements(self, requirements_file_path=REQUIREMENTS_TXT):
        """
        Get and add the requirements

        :param requirements_file_path: path to input file
        """
        self.data['requirements']['run'] += get_requirements(requirements_file_path)
        self.data['requirements']['run'] += get_submodules()

    def add_description(self, description_file_path=DESCRIPTION_MD):
        """
        Get and add the exhaustive description

        :param description_file_path: path to input file
        """
        self.data['about']['description'] = get_description(description_file_path)

    def add_packages(self):
        """ Get and add the subpackages """
        self.data['packages'] = get_packages(self.data['package']['name'])

    def add_test_sources(self):
        """ Get and add the subpackages """
        self.data['test']['source_files'] = get_test_dirs(self.data['package']['name'])
        command = 'python ' + ' '.join(self.data['test']['source_files'])
        self.data['test']['commands'] = [command]

    # write the files derived from the metadata assuming all necessary data is loaded

    def write_conda_build_recipe_meta_yaml(self, output_file_path=CONDA_BUILD_RECIPE_META_YAML):
        """
        Write the conda build recipe meta.yaml file based on the metadata

        :param (str) output_file_path: path to desired output file
        """
        with open(output_file_path, 'w') as output_file:
            yaml.safe_dump(self.data, output_file, sort_keys=False)

    def write_setup_py(self, output_file_path=SETUP_PY, template_file_path=TEMPLATE_SETUP_PY):
        """
        Write the setup.py file based on the metadata using a template file

        :param (str) output_file_path: path to desired output file
        :param (str) template_file_path: path to setup.py template file,
                                         if None (default) set to template in development_tools module
        """
        content = {f'{key0}_{key1}': self.data[key0][key1] for key0, key1 in CONTENT_SETUP}
        content["packages"] = self.data["packages"]

        # adjust python requirement
        content.update({'python_requires': self.data['requirements']['run'][0].strip('python')})

        with open(template_file_path, 'r') as template_file:  # Read and fill template
            src = Template(template_file.read())
            result = src.substitute(content)

        with open(output_file_path, 'w') as output_file:  # Write output file
            output_file.write(result)

    def write_citation_cff(self, output_file_path=CITATION_CFF, template_file_path=TEMPLATE_CITATION_CFF):
        """
        Write the CITATION.cff file based on the metadata using a template file

        :param (str) output_file_path: path to desired output file
        :param (str) template_file_path: path to CITATION.cff template file,
                                         if None (default) set to template in development_tools module
        """
        content = {f'{key0}_{key1}': self.data[key0][key1] for key0, key1 in CONTENT_CITATION}

        # restructure keywords
        content['about_keywords'] = '  - ' + '\n  - '.join(f'"{keyword}"' for keyword in content['about_keywords'])

        with open(template_file_path, 'r') as template_file:  # Read and fill template
            src = Template(template_file.read())
            result = src.substitute(content)

        with open(output_file_path, 'w') as output_file:  # Write output file
            output_file.write(result)

    def write_readme_md(self, output_file_path=README_MD, template_file_path=TEMPLATE_README_MD):
        """
        Write the README.md file based on the metadata using a template file

        :param (str) output_file_path: path to desired output file
        :param (str) template_file_path: path to README.md template file,
                                         if None (default) set to template in development_tools module
        """
        content = {f'{key0}_{key1}': self.data[key0][key1] for key0, key1 in CONTENT_README}
        content.update(year=datetime.today().strftime('%Y'))

        with open(template_file_path, 'r') as template_file:  # Read and fill template
            src = Template(template_file.read())
            result = src.substitute(content)

        with open(output_file_path, 'w') as output_file:  # Write output file
            output_file.write(result)

    def write_docs_conf_py(self, output_file_path=DOCS_CONF_PY, template_file_path=TEMPLATE_CONF_PY):
        """
        Write the conf.py file based on the metadata using a template file

        :param (str) output_file_path: path to desired output file
        :param (str) template_file_path: path to conf.py template file,
                                         if None (default) set to template in development_tools module
        """
        content = {f'{key0}_{key1}': self.data[key0][key1] for key0, key1 in CONTENT_CONF}
        content.update(year=datetime.today().strftime('%Y'))

        with open(template_file_path, 'r') as template_file:  # Read and fill template
            src = Template(template_file.read())
            result = src.substitute(content)

        with open(output_file_path, 'w') as output_file:  # Write output file
            output_file.write(result)

    def write_docs_index_header_rst(self, output_file_path=DOCS_INDEX_HEADER_RST, template_file_path=TEMPLATE_INDEX_HEADER_RST):
        """
        Write the index_header.rst file based on the metadata using a template file

        :param (str) output_file_path: path to desired output file
        :param (str) template_file_path: path to index_header.rst template file,
                                         if None (default) set to template in development_tools module
        """
        content = {f'{key0}_{key1}': self.data[key0][key1] for key0, key1 in CONTENT_INDEX_HEADER}
        content['about_title'] += "\n" + "=" * len(content['about_title'])

        with open(template_file_path, 'r') as template_file:  # Read and fill template
            src = Template(template_file.read())
            result = src.substitute(content)

        with open(output_file_path, 'w') as output_file:  # Write output file
            output_file.write(result)


# helper functions

def get_python_version(local_settings_path):
    """
    Get the python version from the file

    :param (str) local_settings_path: path to local settings.sh file
    """
    for settings_path in [local_settings_path, DEV_TOOLS_ENV_SETTINGS_SH]:
        with open(settings_path, 'r') as settings_file:
            lines = settings_file.readlines()
        for line in lines:
            if line.startswith('PYTHON_VERSION'):
                python_version = line.split('=')[1].strip()
                return f"python>={python_version}"

def get_requirements(requirements_file_path):
    """
    Get the requirements from the file

    :param (str) requirements_file_path: path to requirements.txt file
    """
    with open(requirements_file_path, 'r') as requirements_file:
        lines = requirements_file.readlines()
    return [line.strip() for line in lines if not line.startswith("#")]

def get_submodules():
    """ Get the submodules of the project """
    submodules = os.listdir(MODULES_DIR)
    submodules.remove("development_tools")
    for i, submodule in enumerate(submodules):
        metadata = Metadata(MODULES_DIR + f"{submodule}/" + METADATA_YAML)
        version = metadata.data['package']['version']
        submodules[i] += f"=={version}"  # fix version of submodule to latest released one
    return submodules

def get_description(description_file_path):
    """
    Get the description from the file

    :param (str) description_file_path: path to file with the description
    """
    with open(description_file_path, 'r') as description_file:
        lines = description_file.readlines()
    description = ''.join(lines).strip()
    return description

def get_packages(package_path):
    """
    Go to the path and recognize and return all submodules (i.e. have __init__.py) as list

    :param (str) package_path: path to package folder
    """
    packages = [package_path]  # name of package, must be equal to path
    for module in os.listdir(package_path):  # find all submodules
        path_to_subdir = os.path.join(package_path, module)
        if os.path.isdir(path_to_subdir) and '__init__.py' in os.listdir(path_to_subdir):
            packages.append(package_path + '.' + module)
    return sorted(packages)

def get_test_dirs(package_path):
    """
    Go to the path and recognize and return all Python test files, i.e., those that start with 'test_'

    :param (str) package_path: path to package folder
    """
    exclude = ["envs", "modules", "Lib", "lib", "pkgs"]  # if environment is installed locally
    test_dirs = set()
    for root, dirs, files in os.walk(package_path + "/../"):
        dirs[:] = [d for d in dirs if d not in exclude and not d.startswith(".")]
        for file in files:
            if file.startswith("test_") and file.endswith(".py"):
                test_dir = root.replace(package_path + "/../", "")
                test_dirs.add(test_dir)
    test_dirs = [test_dir.replace("\\", "/") + "/test_*.py" for test_dir in sorted(test_dirs)]
    return test_dirs


# final function

def write_all_metadata_files(output_path=""):
    """
    write all files derived from the metadata

    :param output_path: path to the directory where the files are created,
                        by default in the top repository (apart from the conda build recipe)
    :return:
    """
    metadata = Metadata()
    metadata.add_conda_build_recipe_base()
    metadata.add_python_version()
    metadata.add_requirements()
    metadata.add_packages()
    metadata.add_description()
    metadata.add_test_sources()

    if output_path and not os.path.exists(output_path):
        os.makedirs(output_path)

    metadata.write_setup_py(output_path + SETUP_PY)
    metadata.write_citation_cff(output_path + CITATION_CFF)
    metadata.write_readme_md(output_path + README_MD)

    if not output_path:
        output_path = CONDA_BUILD_RECIPE_DIR
    if not os.path.exists(output_path):
        os.makedirs(output_path)

    metadata.write_conda_build_recipe_meta_yaml(output_path + META_YAML)


if __name__ == "__main__":
    write_all_metadata_files(*sys.argv[1:])
