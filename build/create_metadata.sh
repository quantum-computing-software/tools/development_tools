#!/usr/bin/env bash

################################################################################
# Create the metadata files
################################################################################

# get and go to the repo root
REPO=$(git rev-parse --show-superproject-working-tree --show-toplevel | head -1)
cd $REPO


# call the python script to create all files and pass all options
python modules/development_tools/build/metadata.py "$@"
