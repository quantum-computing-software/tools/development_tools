""" setup for project development_tools """

from setuptools import setup, find_packages


setup(name = 'development_tools',
      version = '0.1',
      url = 'https://gitlab.com/quantum-computing-software/tools/development_tools',
      author = 'DLR-SC',
      author_email = 'qc-software@dlr.de',
      python_requires = '>=3.10',
      description = 'development tools for our QC projects',
      packages = find_packages(include=['development_tools']),
      zip_safe = False)
