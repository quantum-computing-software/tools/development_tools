## Execute local coverage / linting check before each commit 

**Set up and use Git Hooks**

In order to test the qualitiy of the code before commit/push it is possible to use the scripts 
provided in the `modules/development_tools/check` folder.
Just execute `bash check` in order to run the pylint and coverage test and 
print the results. 

By running 
```
    bash modules/development_tools/git/add_hooks.sh
``` 

you can install so-called **pre-commit git hooks** and the checks will be run automatically with each commit. 

If you want to bypass the automatic checks before commit you can run the git commit command 
with the `--no-verify` flag, thus

```
    git commit --no-verify
```

Another option is to deselect hooks when committing via PyCharm.


**Clean up Git Hooks**

To remove the git hooks simply execute
```
    bash modules/development_tools/git/remove_hooks.sh
```


