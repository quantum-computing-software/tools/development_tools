#!/usr/bin/env bash
#
# bash script to delete git hooks
#

# clean the git hooks
function remove_pre_commit_hook() {
      cd $(git rev-parse --show-toplevel)
      /bin/rm .git/hooks/pre-commit
}

remove_pre_commit_hook

