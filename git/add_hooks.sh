#!/usr/bin/env bash
# bash script to install git pre-commit hook
# the check script will be added to the local .git folder
# as a pre-commit hooks meaning it will be executed with every git commit 

function create_pre_commit_hook() {
	    cd $(git rev-parse --show-toplevel)
	    chmod +x check
	    cp check .git/hooks/pre-commit
	    chmod +x .git/hooks/pre-commit
	    }

create_pre_commit_hook
