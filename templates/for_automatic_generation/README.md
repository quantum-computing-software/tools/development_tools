# $about_title

[![pipeline status]($about_home/badges/development/pipeline.svg)]($about_home-/commits/development)

$about_summary

## Documentation

The full documentation can be found [here]($about_doc_url).

#$about_description

## License

This project is [$about_license]($about_home-/blob/development/LICENSE) licensed.

Copyright © $year $author_affiliation. 

Please find the individual contributors [here]($about_home-/blob/development/CONTRIBUTORS) 
and information for citing this package [here]($about_home-/blob/development/CITATION.cff).
