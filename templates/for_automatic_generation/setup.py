""" setup for project quark """

from setuptools import setup, find_packages


setup(name = '$package_name',
      version = '$package_version',
      url = '$about_home',
      author = '$author_name',
      author_email = '$author_email',
      python_requires = '$python_requires',
      description = '$about_summary',
      packages = find_packages(include=$packages),
      zip_safe = False)
