#!/usr/bin/env bash

################################################################################
# Create the documentation
################################################################################

# get and go to the repo root
REPO=$(git rev-parse --show-superproject-working-tree --show-toplevel | head -1)
cd $REPO


if [ "$1" == "" ]; then
    OUTPUT_DIR=docs/html/
else
    OUTPUT_DIR=$1
fi


# install requirements to build docs
conda config --add channels conda-forge
conda install --file modules/development_tools/docs/requirements.txt

# create necessary helping files
python modules/development_tools/docs/prepare_docs.py

# switch to build directory
cd modules/development_tools/docs/pages/

# copy rst and md files and tutorials to source directory
cp ../../../../docs/*.md source/
cp ../../../../docs/src/*.rst source/
cp -r ../../../../docs/tutorials source/

# add the generated header to the index
cat source/index_header.rst source/index.rst > index.rst
cp index.rst source/
rm source/index_header.rst

# create the html website
make html

# move website files to public folder
cd ../../../..
mv modules/development_tools/docs/pages/build/html/ "$OUTPUT_DIR"

# clean up
find modules/development_tools/docs/pages/ ! -name ".gitkeep" ! -name "Makefile" \( ! -type d -o -empty \) -delete
