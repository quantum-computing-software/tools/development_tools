""" script for automatic creation of all metadata files """

from modules.development_tools.build.metadata import Metadata


if __name__ == "__main__":
    metadata = Metadata()
    metadata.write_docs_conf_py()
    metadata.write_docs_index_header_rst()
