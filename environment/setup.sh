#!/usr/bin/env bash

################################################################################
# Run environment setup
#
# Options:
#     setup.sh [local] [--env_name <name_of_environment>]
# or
#     setup.sh --env_dir <environment_directory>
################################################################################

# script options and parameters
OPTION_LOCAL="local"
PARAMETER_ENV_DIR="--env_dir"
PARAMETER_ENV_NAME="--env_name"
PROVIDED_ENV_DIR=""
PROVIDED_ENV_NAME=""


# get and go to the repo root
REPO=$(git rev-parse --show-superproject-working-tree --show-toplevel | head -1)
cd $REPO


# get the functions and global settings of the development_tools
source modules/development_tools/environment/funcs.sh
source modules/development_tools/environment/settings.sh
source modules/development_tools/build/funcs.sh
source modules/development_tools/build/settings.sh

# get the (local, possibly overwriting) project settings
SETTINGS=dev_tools/settings.sh
if [ -f "$SETTINGS" ]; then
    source $SETTINGS
else
    echo "$SETTINGS does not exist."
fi


# if there are parameters given we loop through them
while [ "$1" != "" ]; do
    # check if the given parameters fit to a run option
    # and set (resp. overwrite) variables accordingly
    case "$1" in
        "$OPTION_LOCAL")       LOCAL=true;;
        "$PARAMETER_ENV_DIR")  shift
                               PROVIDED_ENV_DIR=$1;;
        "$PARAMETER_ENV_NAME") shift
                               PROVIDED_ENV_NAME=$1;;
        *) echo "$1 is unknown input"
    esac
    # shift all the parameters down by one
    shift
done


# get the full environment dir if not provided
if [ "$PROVIDED_ENV_DIR" != "" ]; then
    FULL_ENV_DIR=$PROVIDED_ENV_DIR
else
    get_package "$METADATA_FILE"
    get_full_env_dir "$PROVIDED_ENV_NAME" "$PACKAGE" "$ENV_DIR" "$LOCAL"
fi

# call setup function
setup_env "$FULL_ENV_DIR" "$PYTHON_VERSION"
