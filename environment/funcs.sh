#!/usr/bin/env bash
#
# functions for creating the environment
#

function get_full_env_dir() {
    # Build the environment path from the settings if not provided

    ENV_NAME=$1
    PACKAGE=$2
    ENV_DIR=$3
    LOCAL=$4

    # create default environment name if the name is not provided
    if [ "$ENV_NAME" == "" ]; then
        if [ "$PACKAGE" == "" ]; then
            # some default name
            ENV_NAME="dev"
        else
            # per default we take the project name
            ENV_NAME="${PACKAGE}"
        fi
        # and append the current date
        ENV_NAME="${ENV_NAME}_$(date +%F)"
    fi

    # if local option is used the path is relative to the repo root
    FULL_ENV_DIR=$ENV_DIR/$ENV_NAME
    if [ "$LOCAL" != true ]; then
        # otherwise it will be "globally" in the conda base repo
        FULL_ENV_DIR=$(conda info --base)/$FULL_ENV_DIR
    fi
}

function setup_env() {
    # set the environment up

    ENV_DIR=$1
    PYTHON_VERSION=$2

    create_env "$ENV_DIR" "$PYTHON_VERSION"
    install_requirements "$ENV_DIR"
}

function create_env() {
    # create the environment

    ENV_DIR=$1
    PYTHON_VERSION=$2

    # add the channel to the configuration to find all packages
    conda config --add channels conda-forge

    conda create --prefix "$ENV_DIR" python="$PYTHON_VERSION"
}

function install_requirements() {
    # install packages in environment

    ENV_DIR=$1

    # needed to use 'conda activate' in a bash script
    eval "$(conda shell.bash hook)"
    conda activate "$ENV_DIR"

    # check if activation actually worked otherwise conda will unintentionally install in base environment
    if [[ $CONDA_DEFAULT_ENV == "base" ]] || [[ $ENV_DIR != *"$CONDA_DEFAULT_ENV" ]]; then
        echo "Activation of environment failed, something went wrong during setup"
        exit 1
    else
        set -eo pipefail  # exit with error if any command fails

        printf "\n\n~~~ Install project in editable mode and its requirements ~~~\n\n"
        if [ -f "./requirements.txt" ]; then
            # put all together in one installation to reduce time for resolving environment
            conda install conda --file ./requirements.txt --file ./modules/development_tools/requirements.txt --yes
        else
            conda install conda --file ./modules/development_tools/requirements.txt --yes
        fi

        # install package itself in development mode
        conda develop ./

        install_submodules_conda

        install_requirements_pip
    fi
}

function install_submodules_conda() {
    # go into modules and install them with their requirements

    if [ -d "./modules" ]; then
        for MODULE in ./modules/*; do

            if [ "$(ls -A "$MODULE")" ]; then
                cd $MODULE

                if [[ $MODULE != "./modules/development_tools" ]]; then
                    printf "\n\n~~~ Install $MODULE in editable mode ~~~\n\n"
                    if [ -f "./requirements.txt" ]; then
                        conda install --file ./requirements.txt --yes
                    fi
                    conda develop ./
                fi

                cd ../../
            else
                printf "\n\nWARNING: Cannot install $MODULE as directory is empty, please use 'git submodule update --init' to get submodules\n\n"
            fi

        done
    fi
}

function install_requirements_pip() {
    # go into modules and install their pip requirements

    printf "\n\n~~~ Install remaining pip requirements ~~~\n\n"
    if [ -f "./requirements_pip.txt" ]; then
        pip install -r ./requirements_pip.txt
    fi

    if [ -d "./modules" ]; then
        for MODULE in ./modules/*; do
            if [ -f "$MODULE/requirements_pip.txt" ]; then
                pip install -r "$MODULE/requirements_pip.txt"
            fi
        done
    fi
}

# further functionality for removing or cleaning up environment

function remove_env() {
    # clean the environment

    ENV_DIR=$1

    conda env remove --prefix "$ENV_DIR"
}

function uninstall_package() {
    # uninstall packages in environment

    ENV_DIR=$1
    PACKAGE=$2

    # needed to use 'conda activate' in a bash script
    eval "$(conda shell.bash hook)"
    conda activate "$ENV_DIR"

    # check if activation actually worked otherwise pip will uninstall in base environment
    if [[ $CONDA_DEFAULT_ENV == "base" ]]; then
        echo 'Activation of environment failed, cannot uninstall package'
    else
        conda remove --yes "$PACKAGE"
    fi
}

function clean_package() {
    # clean packages in environment

    ENV_DIR=$1
    PACKAGE=$2

    uninstall_package "$ENV_DIR" "$PACKAGE"
    rm -rf "./$PACKAGE.egg-in"
}

