#!/usr/bin/env bash
#
# general settings for setup
#

# python version
PYTHON_VERSION=3.11

# relative path for environment directory,
# by default will be in the conda installation repository,
# (as this allows conda to find the environment globally)
ENV_DIR=envs

# by default the environment is not installed locally
LOCAL=false
