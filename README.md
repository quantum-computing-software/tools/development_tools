# Development Tools

The repository contains tools which support the development of quantum computing software projects of DLR-SC:

* determine the default structure of a QC project,
* setup of a project specific conda environment with current python version and all required packages,
* check scripts for automated checking of tests, code coverage, coding style etc.,
* build scripts for automated packaging and documentation generation,
* script to configure git hooks.

## Preparation

Include this repository as a submodule (use relative paths for url) in your project repository. 
By default, it is assumed that the submodule lies in a folder under the repository root named `modules/`. 

Afterwards, you can copy the content of the `templates/project_structure/` folder into your repository root folder,
rename the folder `(short)_project_name/` to the corresponding name of your package,
and replace all appearances of the term in the `METADATA.yaml` accordingly.
If you use a short project name, e.g. an abbreviation, as the package name (and not the full root repository name), 
this needs to be used in the `METADATA.yaml` as package name as well.
The `METADATA.yaml` file is the single point of truth for all the metadata information. 

Following our standards your repository root should have the following structure and contain the following files:
* `(short)_project_name/`
    * all source code
* `dev_tools/`
    * `.gitlab-ci.yml` - definition of the gitlab CI
    * `settings.sh` - local settings overwriting the ones from `development_tools` module
* `modules/`
    * `development_tools/` - this package
    * all other required qc projects as submodules (no recursion)
* `tests/`
    * tests for the code in the same folder structure
* `.gitignore`
* `.gitmodules`
* `check` - script to check different parts of the code (can only be used, if development_tools are initialized)
* `CONTRIBUTORS` - list of all people who have contributed to the package
* `LICENSE` - the license file
* `METADATA.yaml` - single point of truth for metadata information
* `requirements.txt` - conda requirements
* `setup` - script to set up development environment (can only be used, if development_tools are initialized)

Optionally you can also include:
* `docs` - optional folder for documentation
  + `tutorials` - jupyter notebooks showing usage of your package 
  + `src` - source files for documentation
  + other files explaining the usage of the package
* `requirements_pip.txt` - optional requirements for pip, if there is a package that cannot be installed with conda


## Usage 

### Environment

:information_source: It is best practice to use virtual environments, for further information see 
[Conda Docs](https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#).

First make sure all submodules are initialized by 
```
git submodule init
git submodule update
```  

To setup a new development environment you can now use in your repository
```
bash setup [local] [--env_name <name_of_environment>]
```  

* without flags the environment will be created 
    * with the default values defined in `modules/development_tools/environment/settings.sh`
    * local settings defined in `dev_tools/settings.sh` will overwrite the default ones in `environment/settings.sh`
* the name of the environment is defined
    * with the flag `--env_name <name_of_environment>` or the setting `ENV_NAME=<name_of_environment>` in the settings file
    * otherwise it will be by default the project name followed by '_dev' 
* using the `local` flag or the setting `LOCAL=true` in the settings file will place the environment in the repository under `envs/env_name` 
  in your repository root instead of the default, which is defined by conda 
  (might be the installation folder or somewhere in your user space). 

This will install a conda environment with 
* python 3.10,
* the required pip packages to use the checks,
* your project with its required pip packages and 
* the submodules with their required pip packages (only one depth of recursion!)

This is based on the scripts implemented in the `environment/` folder. 

You can activate the environment with 
```
conda activate <name_of_environment>
```
if you installed it non-locally, otherwise in the repository root use 
```
conda activate envs/<name_of_environment>
```

### Checks

To run the checks on your code after activating the environment you can then use
```
bash check [pytest] [coverage] [pylint] [tutorials]
```
* use one or more flags to specify which specific tests are run
* if no flags are used `coverage` and `pylint` will be run by default 
* the options are 
    * `pytest` - to see whether the tests run through,
    * `coverage` - to see whether the code coverage is sufficient,
    * `pylint` - to see if the style is compliant with our regulations, or
    * `tutorials`- to run the example jupyter notebooks.

This will print the results in the command line but also create a `reports/` folder where you can find the results of the runs. 

These checks are also used in our continuous integration, 
which will be run every time you push something to this repository. 

This is based on the scripts implemented in the `checks/` folder. 

### Build Scripts 

You can use 
```
bash modules/development_tools/build/check_version.sh
```
to compare your current version against the one published on the channel,
```
bash modules/development_tools/build/create_metadata.sh
```
to generate all necessary metadata files based on the `METADATA.yaml`, and
```
bash modules/development_tools/docs/create_docs.sh
```
to build the documentation.

Those can also be included in the CI.

### Git Hooks

To add the checks to the git commit command, such they are tested everytime before you commit, you can use
```
bash modules/development_tools/git/add_hooks.sh
```
to install git hooks.
It is still possible to commit without using the hooks by
```
git commit --no-verify
```

You can use the following to deinstall the hooks
```
bash modules/development_tools/git/remove_hooks.sh
```
