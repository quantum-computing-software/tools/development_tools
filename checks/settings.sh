#!/usr/bin/env bash
#
# general settings for checks
#

# folder(s) where to search for tests
TEST_DIRS="tests/"

# folder(s) where to search for tutorial notebooks
TUTORIALS_DIRS="docs/tutorials/"

# specific pylint settings file
SETTINGS_FILE_LINTING="modules/development_tools/checks/.pylintrc"

# file names for reports
REPORTS_DIR="reports"
REPORT_FILE_TESTING="testing.txt"
REPORT_FILE_LINTING="linting.txt"
REPORT_FILE_COVERAGE="coverage.txt"

# scores
SCORE_LINTING=9.9
SCORE_COVERAGE=99
