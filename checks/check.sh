#!/usr/bin/env bash

################################################################################
# Run multiple checks
#
# Options:
#     check.sh [pytest] [coverage] [pylint] [tutorials]
################################################################################

# all possible options for this script
OPTION_PYTEST="pytest"
OPTION_PYLINT="pylint"
OPTION_COVERAGE="coverage"
OPTION_TUTORIALS="tutorials"

RUN_PYTEST=false
RUN_COVERAGE=false
RUN_PYLINT=false
RUN_TUTORIALS=false


# get and go to the repo root
REPO=$(git rev-parse --show-superproject-working-tree --show-toplevel | head -1)
cd $REPO


# get the functions and global settings of the development_tools
source modules/development_tools/checks/funcs.sh
source modules/development_tools/checks/settings.sh
source modules/development_tools/build/funcs.sh
source modules/development_tools/build/settings.sh

# get the (local, possibly overwriting) project settings
SETTINGS=dev_tools/settings.sh
if [ -f "$SETTINGS" ]; then
    source $SETTINGS
else
    echo "$SETTINGS does not exist."
fi


# check for positional parameters
if [ "$1" == "" ]; then
    # if no parameters are given we check the coverage and pylint
    RUN_COVERAGE=true
    RUN_PYLINT=true
else
    # if there are parameters given we loop through them
    while [ "$1" != "" ]; do
        case "$1" in
            # check if the given parameters fit to a run option
            # and set variables accordingly
            "$OPTION_PYTEST") RUN_PYTEST=true;;
            "$OPTION_COVERAGE") RUN_COVERAGE=true;;
            "$OPTION_PYLINT") RUN_PYLINT=true;;
            "$OPTION_TUTORIALS") RUN_TUTORIALS=true;;
            *"/") echo "set reports dir to $1"
                  REPORTS_DIR="$1";;
            *) echo "set report file to $1"
               REPORT_FILE="$1";;
        esac
        # shift all the parameters down by one
        shift
    done
fi


get_package "$METADATA_FILE"

# set report files
if [ "$REPORT_FILE" == "" ]; then
    REPORT_FILE_T="$REPORTS_DIR/$REPORT_FILE_TESTING"
    REPORT_FILE_L="$REPORTS_DIR/$REPORT_FILE_LINTING"
    REPORT_FILE_C="$REPORTS_DIR/$REPORT_FILE_COVERAGE"

    # create the reports directory if not present already
    mkdir -p "$REPORTS_DIR"
else
    REPORT_FILE_T="$REPORT_FILE"
    REPORT_FILE_L="$REPORT_FILE"
    REPORT_FILE_C="$REPORT_FILE"
fi

# call the functions according to the provided options

set -eo pipefail  # exit with error if any command fails

if $RUN_PYTEST; then
    # run pytest
    check_pytest "$TEST_DIRS" "$REPORT_FILE_T"
fi

if $RUN_COVERAGE; then
    # run pytest with coverage
    check_coverage "$PACKAGE" "$TEST_DIRS" "$SCORE_COVERAGE" "$REPORT_FILE_C"
fi

if $RUN_PYLINT; then
    # run pylint to check style
    check_pylint "$PACKAGE" "$TEST_DIRS" "$SCORE_LINTING" "$SETTINGS_FILE_LINTING" "$REPORT_FILE_L"
fi

if $RUN_TUTORIALS; then
    # run tutorials
    check_tutorials "$TUTORIALS_DIRS"
fi
