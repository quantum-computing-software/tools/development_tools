#!/usr/bin/env bash
#
# functions for checks
#

function check_pytest() {
    TEST_DIRS=$1
    REPORT_FILE=$2

    echo $'\n~~~~~ Running pytest ~~~~~~~~~~~~~~~~~~~~~~~~~\n'
    pytest $TEST_DIRS --no-cov -W error -W ignore:"linked SCIP":UserWarning | tee "$REPORT_FILE"
    SCORE=$([ $(cat $REPORT_FILE | grep -oP '\d+\s+(errors|failed)' | grep -oP -m 1 '[^\s+(errors|failed)]*') ] && echo "1" || echo "0")
    echo "score: $SCORE"
    if [ $(echo "$SCORE" '!=' "0" | bc -l) -eq 1 ]; then
        echo "Pytest failed!"
        exit 1
    else
        echo "Pytest succeeded!"
    fi
}

function check_coverage() {
    PROJECT=$1
    TEST_DIRS=$2
    EXPECTED_SCORE=$3
    REPORT_FILE=$4

    echo $'\n~~~~~ Running pytest with coverage ~~~~~~~~~~~\n'
    pytest --cov="$PROJECT" --cov-report=term-missing --disable-warnings $TEST_DIRS | tee "$REPORT_FILE"
    SCORE=$(cat "$REPORT_FILE" | grep -oP '^TOTAL\s+\d+\s+\d+\s+(\d+\%)$' | grep -oP '[^ ]*%' | grep -oP '[^%]*')
    echo "score: $SCORE"
    if [ $(echo "$SCORE" '<' "$EXPECTED_SCORE" | bc -l) -eq 1 ]; then
        echo "Pytest/Coverage score is too low!"
        exit 1
    else
        echo "Coverage score is fine!"
    fi
}

function check_pylint() {
    PROJECT=$1
    TEST_DIRS=$2
    EXPECTED_SCORE=$3
    SETTINGS_FILE=$4
    REPORT_FILE=$5

    PYLINT_DIRS="$PROJECT $TEST_DIRS"
    if [[ "$TEST_DIRS" == *"$PROJECT"* ]]; then
        # the test directories are contained in the project thus we only need this
        PYLINT_DIRS="$PROJECT"
    fi

    echo $'\n~~~~~ Running pylint ~~~~~~~~~~~~~~~~~~~~~~~~~\n'
    pylint $PYLINT_DIRS --exit-zero --rcfile="$SETTINGS_FILE" --output-format=text | tee "$REPORT_FILE"
    SCORE=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' "$REPORT_FILE")
    echo "score: $SCORE"
    if [ $(echo "$SCORE" '<' "$EXPECTED_SCORE" | bc -l) -eq 1 ]; then
        echo "Pylint score is too low!"
        exit 1
    else
        echo "Pylint score is fine!"
    fi
}

function check_tutorials() {
    TUTORIALS_DIRS=$1

    echo $'\n~~~~~ Running tutorials ~~~~~~~~~~~~~~~~~~~~~~\n'
    pytest --nbval --disable-warnings $TUTORIALS_DIRS
}
